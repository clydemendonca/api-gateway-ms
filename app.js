var express = require('express');

var routes = require('./routes.js');

var app = express();
routes.setRoutesForApplication(app);

app.listen(3000, function() {
  console.log('API Gateway Listening on port 4000');
});
