module.exports.setRoutesForApplication = function(app){

  app.get('/users/:userId', function (req, res) {
    res.send('Hello World ' + req.params.userId);
  });

  app.post('/users', function (req, res) {
    res.send('Hello World');
  });

  app.put('/users/:userId', function (req, res) {
    res.send('Hello World');
  });

};
